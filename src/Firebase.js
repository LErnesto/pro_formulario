// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, connectAuthEmulator } from "firebase/auth";
import { getFirestore, connectFirestoreEmulator  } from "firebase/firestore"
import {getStorage, connectStorageEmulator  } from "firebase/storage"


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBdb5YL9DGc1KDPgoI_RwiFrPCH_xDQ9pw",
  authDomain: "proyecto-ob.firebaseapp.com",
  projectId: "proyecto-ob",
  storageBucket: "proyecto-ob.appspot.com",
  messagingSenderId: "1063466224557",
  appId: "1:1063466224557:web:8a64cce0ec76091c64af24"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth();
//connectAuthEmulator(auth, "http://localhost:9099");
export const db = getFirestore();
//connectFirestoreEmulator(db, 'localhost', 8060);
export const db_file = getStorage();
//connectStorageEmulator(db_file, 'localhost', 9199);