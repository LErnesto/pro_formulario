import { useEffect, useState } from 'react';
import './App.css';
import Login from './componentes/login/Login';
import Registro from './componentes/Registro/Registro';
import Sistema from './componentes/sistema/Sistema';
import {BrowserRouter as Browser, Route, Switch, Redirect } from "react-router-dom";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "./Firebase";
import { hayUsuarioLogeado } from "./FuncionesFirebase";

function App() {
  const [usuario,setUsuario] = useState(null);

  useEffect(()=>{
    onAuthStateChanged(auth, async (user) => {
      if (user) {
        const datosUsuario = await hayUsuarioLogeado(user);
        setUsuario(datosUsuario);
      }else{
        setUsuario(null);
      }
    });
  },[]);

  return (
    <Browser>
      <div className="App">
        <Switch>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/registro" component={Registro}/>
          <Route path="/"><Sistema usuario={usuario}/></Route>
        </Switch>
      </div>
      {usuario && <Redirect to="/"/>}
    </Browser>
  );
}

export default App;
