import { fireEvent, render, screen } from "@testing-library/react";
import Menu from "./Menu";

const inspector = jest.fn();
beforeEach(()=>{
    render(<Menu usuario={{nombre:'Ernesto'}} setPagina={inspector} setLista={inspector}/>);
});

describe('renderizado del menu',()=>{
    it('componentes del menu',()=>{
        const titulo = screen.getByRole('heading');
        expect(titulo.textContent).toBe('Bienvenido Ernesto');
        const btns = screen.getAllByRole('button');
        expect(btns.length).toBe(3);
    });
});