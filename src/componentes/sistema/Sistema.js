import React from "react";
import FormularioAspirante from "./formularios/FormularioNuevo";
import ListaRegistros from "./registros/ListaRegistros";
import FormularioConsulta from "./formularios/FormularioConsulta";
import Menu from "./Menu";
import "./Sistema.css";
import { Route, Redirect } from "react-router-dom";

const Sistema = (props)=>{

    if (props.usuario !== null)
    return<div className="sistema">
        <Menu usuario={props.usuario}/>
        <div className="contenedor">
            <Route exact path="/nuevo"><FormularioAspirante id={props.usuario.id} /></Route>
            <Route exact path="/"><ListaRegistros id={props.usuario.id}/></Route>
            <Route exact path="/consulta/:idAspirante"><FormularioConsulta id= {props.usuario.id}/></Route>
        </div>
    </div>
    else
    return <Redirect to="/login"/>
} 

export default Sistema;