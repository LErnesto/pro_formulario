import React, { useEffect, useState } from "react";
import Item from "./Item";
import { obtenerListaAspirantes } from "../../../FuncionesFirebase";

const ListaRegistros = (props)=>{

    const [listaAspirantes,setListaAspirantes] = useState([]);

    useEffect(()=>{
        const obtenerLista = async()=>{
            const documetos = await obtenerListaAspirantes(props.id);
            setListaAspirantes(documetos);
        }
        obtenerLista();
    },[props,listaAspirantes]);

    return<div className="modulo">
        <table className="table">
            <thead>
                <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Postulación</th>
                <th scope="col">Estado</th>
                </tr>
            </thead>
            <tbody>
                {listaAspirantes.map((aspirante)=>{
                    return <Item aspirante={aspirante} key={aspirante.id} id={props.id}/>
                })}
            </tbody>
        </table>
    </div>
}

export default ListaRegistros;