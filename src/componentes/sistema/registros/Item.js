import React from "react";
import { useHistory } from "react-router-dom";

const Item = (props)=>{
    const history = useHistory();

    const mostrarDatos = ()=>{
        history.push('/consulta/'+props.aspirante.id);
    }

    return<tr className="item">
        <td onClick={mostrarDatos}>{props.aspirante.nombre}</td>
        <td onClick={mostrarDatos}>{props.aspirante.postulacion}</td>
        <td onClick={mostrarDatos}>{props.aspirante.estado}</td>
    </tr>
}

export default Item;