import Item from "./Item";
import { fireEvent, render, screen } from "@testing-library/react";

beforeEach(()=>{
    render(<Item aspirante={{nombre:'Ernesto', postulacion:'programacion', estado:"Tamaulipas"}} id="dsdd8a48d4a4da"/>);
});

describe('componente item',()=>{
    it('datos a mostrar',()=>{
        expect(screen.getByText(/Ernesto/)).toBeInTheDocument();
        expect(screen.getByText(/programacion/)).toBeInTheDocument();
        expect(screen.getByText(/Tamaulipas/)).toBeInTheDocument();
    });
});