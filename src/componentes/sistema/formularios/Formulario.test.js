import { fireEvent, render, screen } from "@testing-library/react";
import FormularioAspirante from "./FormularioNuevo";

let nombre;
let edad;
let estado;
let carrera;
let universidad;
let genero1;
let genero2;
let idioma1;
let idioma2;
let idioma3;
let postulacion1;
let postulacion2;
let postulacion3;
let cv;
const registroConsulta = {
    nombre:'Luis Ernesto',
    edad:'23',
    genero:'hombre',
    estado:'Tamaulipas',
    universidad:'UAT',
    carrera:'ISC',
    idiomas:['Español','Ingles'],
    postulacion:'Programador',
    cv:'www.google.com',
    nombrecv:'unnombre'
}
beforeEach(()=>{
    render(<FormularioAspirante/>);
    nombre = screen.getByLabelText(/nombre/i);
    edad = screen.getByLabelText(/edad/i);
    estado = screen.getByLabelText(/estado/i);
    carrera = screen.getByLabelText(/carrera/i);
    universidad = screen.getByLabelText(/universidad/i);
    genero1 = screen.getByLabelText(/hombre/i);
    genero2 = screen.getByLabelText(/mujer/i);
    idioma1 = screen.getByLabelText(/español/i);
    idioma2 = screen.getByLabelText(/ingles/i);
    idioma3 = screen.getByLabelText(/aleman/i);
    postulacion1 = screen.getByLabelText(/programador/i);
    postulacion2 = screen.getByLabelText(/diseñador/i);
    postulacion3 = screen.getByLabelText(/recursos humanos/i);
    cv = screen.getByLabelText(/Subir CV/i);
});

describe('renderizado de formulario',()=>{
    it('se muestran elementos en pantalla',()=>{
        expect(screen.getAllByRole('textbox').length).toBe(5);
        expect(screen.getAllByRole('radio').length).toBe(5);
        expect(screen.getAllByRole('checkbox').length).toBe(3);
        expect(screen.getAllByRole('button').length).toBe(3);
    });
    it('interaccion limpiar formulario',()=>{
        fireEvent.click(screen.getByText(/limpiar/i));
        expect(nombre.value).toBe('');
        expect(edad.value).toBe('0');
        expect(estado.value).toBe('');
        expect(universidad.value).toBe('');
        expect(carrera.value).toBe('');
        expect(genero1.checked).toBe(false);
        expect(genero2.checked).toBe(false);
        expect(idioma1.checked).toBe(false);
        expect(idioma2.checked).toBe(false);
        expect(idioma3.checked).toBe(false);
        expect(postulacion1.checked).toBe(false);
        expect(postulacion2.checked).toBe(false);
        expect(postulacion3.checked).toBe(false);
    });
    it('entrada de elementos de texto',()=>{
        fireEvent.change(nombre,{target:{value:'Ernesto'}});
        fireEvent.change(edad,{target:{value:'25'}});
        fireEvent.change(estado,{target:{value:'Tam'}});
        fireEvent.change(universidad,{target:{value:'UAT Tampico'}});
        fireEvent.change(carrera,{target:{value:'ISI'}});
        expect(nombre.value).toBe('Ernesto');
        expect(edad.value).toBe('25');
        expect(estado.value).toBe('Tam');
        expect(universidad.value).toBe('UAT Tampico');
        expect(carrera.value).toBe('ISI');
    });
    it('interaccion en elementos de genero',()=>{
        fireEvent.click(genero1);
        expect(genero2.checked).toBe(false);
        fireEvent.click(genero2);
        expect(genero1.checked).toBe(false);
    });
    it('interaccion en elementos de postulacion',()=>{
        fireEvent.click(postulacion1);
        expect(postulacion2.checked).toBe(false);
        expect(postulacion3.checked).toBe(false);
        fireEvent.click(postulacion2);
        expect(postulacion1.checked).toBe(false);
        expect(postulacion3.checked).toBe(false);
        fireEvent.click(postulacion3);
        expect(postulacion2.checked).toBe(false);
        expect(postulacion1.checked).toBe(false);
    });
    it('interaccion con elementos de idioma',()=>{
        fireEvent.click(screen.getByText(/limpiar/i));
        expect(idioma1.checked).toBe(false);
        expect(idioma2.checked).toBe(false);
        expect(idioma3.checked).toBe(false);
        fireEvent.click(idioma1);
        fireEvent.click(idioma3);
        expect(idioma1).toBeChecked();
        expect(idioma3).toBeChecked();
    });
    it('datos incorrectos',()=>{
        fireEvent.click(screen.getByText(/limpiar/i));
        fireEvent.click(screen.getByText('Registrar'));
        expect(nombre.classList).toContain('is-invalid');
        expect(edad.classList).toContain('is-invalid');
        expect(estado.classList).toContain('is-invalid');
        expect(universidad.classList).toContain('is-invalid');
        expect(carrera.classList).toContain('is-invalid');
        expect(genero1.classList).toContain('is-invalid');
        expect(genero2.classList).toContain('is-invalid');
        expect(idioma1.classList).toContain('is-invalid');
        expect(idioma2.classList).toContain('is-invalid');
        expect(idioma3.classList).toContain('is-invalid');
        expect(postulacion1.classList).toContain('is-invalid');
        expect(postulacion2.classList).toContain('is-invalid');
        expect(postulacion3.classList).toContain('is-invalid');
        expect(cv.classList).toContain('is-invalid');
    });
    it('falta agregar archivo cv',()=>{
        fireEvent.click(screen.getByText(/limpiar/i));
        fireEvent.change(nombre,{target:{value:'Luis Ernesto'}});
        fireEvent.change(edad,{target:{value:'25'}});
        fireEvent.change(estado,{target:{value:'Tamaulipas'}});
        fireEvent.change(universidad,{target:{value:'UAT'}});
        fireEvent.change(carrera,{target:{value:'ISC'}});
        fireEvent.click(genero1);
        fireEvent.click(postulacion1);
        fireEvent.click(idioma1);
        fireEvent.click(screen.getByText('Registrar'));
        expect(nombre.classList).not.toContain('is-invalid');
        expect(edad.classList).not.toContain('is-invalid');
        expect(estado.classList).not.toContain('is-invalid');
        expect(universidad.classList).not.toContain('is-invalid');
        expect(carrera.classList).not.toContain('is-invalid');
        expect(genero1.classList).not.toContain('is-invalid');
        expect(genero2.classList).not.toContain('is-invalid');
        expect(idioma1.classList).not.toContain('is-invalid');
        expect(idioma2.classList).not.toContain('is-invalid');
        expect(idioma3.classList).not.toContain('is-invalid');
        expect(postulacion1.classList).not.toContain('is-invalid');
        expect(postulacion2.classList).not.toContain('is-invalid');
        expect(postulacion3.classList).not.toContain('is-invalid');
        expect(cv.classList).toContain('is-invalid');
    });
});