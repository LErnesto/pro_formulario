import React, { useState, useRef, useEffect } from "react";
import { actualizarAspirante, consultaAspirante, eliminarAspirante } from "../../../FuncionesFirebase";
import { useParams, useHistory } from "react-router-dom";


const FormularioConsulta = (props)=>{

    const history = useHistory();
    const {idAspirante} = useParams();
    const [nombre,setNombre] = useState('');
    const [edad,setEdad] = useState(0);
    const [genero,setGenero] = useState('');
    const [estado,setEstado] = useState('');
    const [universidad,setUniversidad] = useState('');
    const [carrera,setCarrera] = useState('');
    const [listaIdiomas,setListaIdiomas] = useState([]);
    const [postulacion,setPostulacion] = useState('');
    const [curriculum,setCurriculum] = useState(null);
    const [urlCurriculum,setUrlCurriculum] = useState('');
    const [nombreCurriculum,setNombreCurriculum] = useState('');
    const [listaErrores,setListaErrores] = useState([]);
    const referenciaInputArchivo = useRef();
    const [toast,setToast] = useState(false);

    useEffect(()=>{
        const obtenerAspirante = async()=>{
            const datosAspirante = await consultaAspirante(props.id, idAspirante);
            setNombre(datosAspirante.nombre);
            setEdad(datosAspirante.edad);
            setGenero(datosAspirante.genero);
            setEstado(datosAspirante.estado);
            setUniversidad(datosAspirante.universidad);
            setCarrera(datosAspirante.carrera);
            setListaIdiomas(datosAspirante.listaIdiomas);
            setPostulacion(datosAspirante.postulacion);
            setNombreCurriculum(datosAspirante.nombreCurriculum);
            setUrlCurriculum(datosAspirante.urlCurriculum);
        }
        obtenerAspirante();
    },[props,idAspirante]);

    const agregarIdioma = (elemento)=>{
        const listaAuxiliar = listaIdiomas;
        if(elemento.checked){
            listaAuxiliar.push(elemento.value);
        }else{
            const indiceElemnto = listaAuxiliar.indexOf(elemento.value);
            listaAuxiliar.splice(indiceElemnto,1);
        }
        setListaIdiomas(listaAuxiliar);
    }

    const editar = async()=>{
        const listaerrores = validarDatos();
        if(listaerrores.length === 0 || (listaerrores.length === 1 && listaerrores.includes("cv"))){
            let resultado;
            const aspirante = {
                nombre:nombre,
                edad:edad,
                genero:genero,
                estado:estado,
                universidad:universidad,
                carrera:carrera,
                listaIdiomas:listaIdiomas,
                postulacion:postulacion,
                urlCurriculum:urlCurriculum,
                nombreCurriculum:nombreCurriculum
            };
            resultado = await actualizarAspirante(props.id,idAspirante,aspirante,curriculum);
            if(resultado.exito){
                setToast(true);
                setTimeout(() => {
                    setToast(false);
                    history.push("/");
                }, 2000);
                referenciaInputArchivo.current.value = "";
            }else{
                alert("Ha ocurrido un error: "+resultado.error);
            }
        }else{
            setListaErrores(listaerrores);
            setTimeout(() => {
                setListaErrores([]);
            }, 2000);
        }
    }

    const validarDatos = ()=>{
        const listaerrores = [];
        if(nombre.trim().length === 0){
            listaerrores.push("nombre");
        }
        if(isNaN(parseInt(edad)) || edad < 15){
            listaerrores.push("edad");
        }
        if(genero.trim().length === 0){
            listaerrores.push("genero");
        }
        if(estado.trim().length === 0){
            listaerrores.push("estado");
        }
        if(universidad.trim().length === 0){
            listaerrores.push("universidad");
        }
        if(carrera.trim().length === 0){
            listaerrores.push("carrera");
        }
        if(listaIdiomas.length === 0){
            listaerrores.push("idiomas");
        }
        if(postulacion.trim().length === 0){
            listaerrores.push("postulacion");
        }
        if(curriculum === null){
            listaerrores.push("cv");
        }
        return listaerrores;
    }

    function verCurriculum(){
        window.open(urlCurriculum);
    }

    async function eliminarRegistro(){
        await eliminarAspirante(props.id,idAspirante,nombreCurriculum);
    }

    return<div className="modulo">
        <form className="m-formulario">
            <legend>Datos Personales</legend>
            {/*nombre */}
            <div className="form-floating mb-3">
            <input 
            type="text" 
            className={listaErrores.indexOf("nombre") !== -1 ? "form-control is-invalid": "form-control"} 
            id="nombre" 
            name="nombre" 
            placeholder="Nombre"
            value={nombre} 
            onChange={e=>{setNombre(e.target.value)}}/>
            {listaErrores.indexOf("nombre") !== -1 ? 
            <label htmlFor="nombre" style={{color:'red'}}>Campo obligatorio</label>:
            <label htmlFor="nombre">Nombre</label>}
            </div>

            <div className="input-group mb-3">
                {/*Edad */}
                <div className="form-floating mb-3">
                    <input 
                    type="text" 
                    className={listaErrores.indexOf("edad") !== -1 ? "form-control is-invalid": "form-control"} 
                    id="edad" 
                    name="edad"
                    placeholder="Edad"
                    value={edad} 
                    onChange={e=>{setEdad(e.target.value)}}/>
                    {listaErrores.indexOf("edad") !== -1 ? 
                    <label htmlFor="edad" style={{color:'red'}}>Campo obligatorio</label>:
                    <label htmlFor="edad">Edad</label>}
                </div>
                {/*Genero */}
                <div className="m-1">
                    <div className="form-check">
                        <input 
                        className={listaErrores.indexOf("genero") !== -1 ? "form-check-input is-invalid": "form-check-input"}
                        type="radio" 
                        name="genero" 
                        id="hombre" 
                        checked={genero === "Hombre" ? true:false}
                        onChange={e=>{setGenero("Hombre")}}/>
                        <label className="form-check-label" htmlFor="hombre">Hombre</label>
                    </div>
                    <div className="form-check">
                        <input 
                        className={listaErrores.indexOf("genero") !== -1 ? "form-check-input is-invalid": "form-check-input"}
                        type="radio" 
                        name="genero" 
                        id="mujer" 
                        checked={genero === "Mujer" ? true:false}
                        onChange={e=>{setGenero("Mujer")}}/>
                        <label className="form-check-label" htmlFor="mujer">Mujer</label>
                    </div>
                </div>
            </div>
            {/*estado */}
            <div className="form-floating mb-3">
            <input 
            type="text" 
            className={listaErrores.indexOf("estado") !== -1 ? "form-control is-invalid": "form-control"}  
            id="estado" 
            name="estado" 
            placeholder="Estado"
            value={estado} 
            onChange={e=>{setEstado(e.target.value)}}/>
            {listaErrores.indexOf("estado") !== -1 ? 
            <label htmlFor="estado" style={{color:'red'}}>Campo obligatorio</label>:
            <label htmlFor="estado">Estado</label>}
            </div>
            {/*universidad */}
            <div className="form-floating mb-3">
            <input 
            type="text" 
            className={listaErrores.indexOf("universidad") !== -1 ? "form-control is-invalid": "form-control"}  
            id="universidad" 
            name="universidad" 
            placeholder="Universidad"
            value={universidad} 
            onChange={e=>{setUniversidad(e.target.value)}}/>
            {listaErrores.indexOf("universidad") !== -1 ? 
            <label htmlFor="universidad" style={{color:'red'}}>Campo obligatorio</label>:
            <label htmlFor="universidad">Universidad</label>}
            </div>
            {/*carrera*/}
            <div className="form-floating mb-3">
            <input 
            type="text" 
            className={listaErrores.indexOf("carrera") !== -1 ? "form-control is-invalid": "form-control"} 
            id="carrera" 
            name="carrera" 
            placeholder="Carrera"
            value={carrera} 
            onChange={e=>{setCarrera(e.target.value)}}/>
            {listaErrores.indexOf("carrera") !== -1 ? 
            <label htmlFor="carrera" style={{color:'red'}}>Campo obligatorio</label>:
            <label htmlFor="carrera">Carrera</label>}
            </div>

            <legend>Idiomas</legend>
            <div className="form-check mb-3">
                <input 
                className={listaErrores.indexOf("idiomas") !== -1 ? "form-check-input is-invalid": "form-check-input"} 
                type="checkbox" 
                value="Ingles" 
                id="en" 
                checked={listaIdiomas.includes("Ingles")}
                onChange={(e)=>{agregarIdioma(e.target)}}/>
                <label className="form-check-label" htmlFor="en">Inglés</label>
            </div>
            <div className="form-check mb-3">
                <input 
                className={listaErrores.indexOf("idiomas") !== -1 ? "form-check-input is-invalid": "form-check-input"}
                type="checkbox" 
                value="Español" 
                id="es" 
                checked={listaIdiomas.includes("Español")}
                onChange={(e)=>{agregarIdioma(e.target)}}/>
                <label className="form-check-label" htmlFor="es">Español</label>
            </div>
            <div className="form-check mb-3">
                <input 
                className={listaErrores.indexOf("idiomas") !== -1 ? "form-check-input is-invalid": "form-check-input"}
                type="checkbox" 
                value="Aleman" 
                id="al" 
                checked={listaIdiomas.includes("Aleman")}
                onChange={(e)=>{agregarIdioma(e.target)}}/>
                <label className="form-check-label" htmlFor="al">Alemán</label>
            </div>
            
            <legend>Postulación</legend>

            <div className="form-check form-check-inline mb-3">
                <input 
                className={listaErrores.indexOf("postulacion") !== -1 ? "form-check-input is-invalid": "form-check-input"}
                type="radio" 
                name="postulacion" 
                id="p_programador" 
                checked={postulacion === "Programador" ? true:false}
                onChange={e=>{setPostulacion("Programador")}}/>
                <label className="form-check-label" htmlFor="p_programador">Programador</label>
            </div>
            <div className="form-check form-check-inline mb-3">
                <input 
                className={listaErrores.indexOf("postulacion") !== -1 ? "form-check-input is-invalid": "form-check-input"} 
                type="radio" 
                name="postulacion" 
                id="p_desing" 
                checked={postulacion === "Diseñador" ? true:false}
                onChange={e=>{setPostulacion("Diseñador")}}/>
                <label className="form-check-label" htmlFor="p_desing">Diseñador</label>
            </div>
            <div className="form-check form-check-inline mb-3">
                <input 
                className={listaErrores.indexOf("postulacion") !== -1 ? "form-check-input is-invalid": "form-check-input"}
                type="radio" 
                name="postulacion" 
                id="p_rh" 
                checked={postulacion === "Recursos Humanos" ? true:false}
                onChange={e=>{setPostulacion("Recursos Humanos")}}/>
                <label className="form-check-label" htmlFor="p_rh">Recursos Humanos</label>
            </div>

            {/*subir cv */}
            <div className="mb-3">
                <label htmlFor="formFile" className="form-label">Subir CV (PDF)</label>
                <input 
                ref={referenciaInputArchivo}
                className={listaErrores.indexOf("cv") !== -1? "form-control is-invalid": "form-control"}
                type="file" 
                accept="application/pdf" 
                id="formFile"
                onChange={e=>setCurriculum(e.target.files[0])}/>
            </div>
        </form>
        <div className="controles">
            <button className="btn btn-dark btn-f" onClick={editar} >Editar</button>
            <button className="btn btn-dark btn-f" onClick={verCurriculum}>Ver Curriculum</button>
            <button className="btn btn-danger" onClick={eliminarRegistro}>Eliminar Aspirante</button>
        </div>
        {/*mensaje emergente */}
        <div className="position-fixed bottom-0 end-0 p-3 mensajeemergente">
            <div className={toast ? "toast align-items-center text-white bg-primary border-0 show":"toast"} role="alert" aria-live="assertive" aria-atomic="true">
            <div className="d-flex">
                <div className="toast-body">
                    Actualización exitosa!!...
                </div>
                <button type="button" className="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
            </div>
            </div>
        </div>
    </div>
}

export default FormularioConsulta;