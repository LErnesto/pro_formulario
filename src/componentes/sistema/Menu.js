import React from "react";
import { auth } from "../../Firebase";
import { useHistory } from "react-router-dom";

const Menu = (props)=>{

    const history = useHistory();

    const cerrarSesion = ()=>{
        auth.signOut();
    }

    return<div className="contenedor menu">
        <h2>{'Bienvenido ' + props.usuario.nombre}</h2>
        <div>
            <button className="btn btn-light" onClick={()=>{history.push("/");}}>Aspirantes</button>
            <button className="btn btn-light" onClick={()=>{history.push("/nuevo");}}>Nuevo Aspirante</button>
            <button className="btn btn-light" onClick={cerrarSesion}>Cerrar Sesión</button>
        </div>
    </div>
} 

export default Menu;