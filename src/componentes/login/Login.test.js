import { fireEvent, render, screen } from "@testing-library/react";
import Login from "./Login";

const inspector = jest.fn();
beforeEach(()=>{
    render(<Login/>);
});

describe('renderizado del inicio de sesion',()=>{
    it('componentes renderizado', ()=>{
        const correo = screen.getByLabelText(/correo/i);
        expect(correo.textContent).toBe('');
        const contraseina = screen.getByLabelText(/contraseña/i);
        expect(contraseina.textContent).toBe('');
        expect(screen.getByText(/iniciar sesion/i)).toBeInTheDocument();
        expect(screen.getByText(/registro/i)).toBeInTheDocument();
    });

    it('validacion de correo invalido',()=>{
        const correo = screen.getByLabelText(/correo/i);
        const contraseina = screen.getByLabelText(/contraseña/i);
        fireEvent.change(correo,{target:{ value:'uncorreo@erroneo'}});
        fireEvent.change(contraseina,{target:{ value: '123456' }});
        const btn = screen.getByText(/iniciar sesion/i);
        fireEvent.click(btn);
        expect(screen.getByText(/correo invalido/i)).toBeInTheDocument();
    });
    it('validacion de contraseña invalida',()=>{
        const correo = screen.getByLabelText(/correo/i);
        const contraseina = screen.getByLabelText(/contraseña/i);
        fireEvent.change(correo,{target:{ value:'uncorreo@erroneo.com'}});
        fireEvent.change(contraseina,{target:{ value: '1234' }});
        const btn = screen.getByText(/iniciar sesion/i);
        fireEvent.click(btn);
        expect(screen.getByText(/contraseña no valida/i)).toBeInTheDocument();
    });
    it('validacion de correo invalido y contraseña no valida',()=>{
        const correo = screen.getByLabelText(/correo/i);
        const contraseina = screen.getByLabelText(/contraseña/i);
        fireEvent.change(correo,{target:{ value:'uncorreo@erroneo'}});
        fireEvent.change(contraseina,{target:{ value: '1234' }});
        const btn = screen.getByText(/iniciar sesion/i);
        fireEvent.click(btn);
        expect(screen.getByText(/correo invalido/i)).toBeInTheDocument();
        expect(screen.getByText(/contraseña no valida/i)).toBeInTheDocument();
    });
});