import React, { useState } from "react";
import './Login.css';
import { iniciarSesionServidor } from "../../FuncionesFirebase";

const Login = (props)=>{

    const [correo,setCorreo] = useState('');
    const [contrasena,setContrasena] = useState('');
    const [hayError,setHayError] = useState(false);
    const [listaErrores,setListaErrores] = useState([]);
    const [mensajeError,setMensajeError] = useState('');
    
    const expresionRegular = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    function validarCorreo(correo){
        return expresionRegular.test(correo);
    }

    const iniciarSesion = async()=>{
        if(!validarCorreo(correo)){
            listaErrores.push("correo");
            setHayError(true);
        }
        if(contrasena.trim().length === 0 || contrasena.trim().length < 6){
            listaErrores.push("contraseña");
            setHayError(true);
        }
        if(listaErrores.length === 0){
            const resultado = await iniciarSesionServidor(correo,contrasena);
            if(resultado.exito){
                setMensajeError(resultado.codigoError);
                setHayError(true);
                quitarMensaje();
            }
        }else{
            quitarMensaje();
        }
    }

    function quitarMensaje(){
        setTimeout(() => {
            setHayError(false);
            setListaErrores([]);
            setMensajeError('');
        }, 2000);
    }

    return <>
            <form className="formulario">
                <div className="formulario-contenido">
                    <div className="control-formulario form-floating mb-3">
                        <input 
                        className={listaErrores.indexOf("correo") !== -1 ? "form-control is-invalid":"form-control"}
                        type="text" 
                        id="correo" 
                        placeholder="Correo"
                        value={correo} 
                        onChange={(e)=>{setCorreo(e.target.value)}}/>
                        {listaErrores.includes("correo") ? 
                        <label htmlFor="correo" style={{color:'red'}}>Correo Inválido</label>:
                        <label htmlFor="correo">Correo</label>}
                    </div>
                    <div className="control-formulario form-floating mb-3">
                        <input 
                        className={listaErrores.indexOf("contraseña") !== -1 ? "form-control is-invalid":"form-control"}
                        type="password" 
                        id="password" 
                        placeholder="Contraseña"
                        value={contrasena} 
                        onChange={(e)=>{setContrasena(e.target.value)}}/>
                        {listaErrores.includes("contraseña") ? 
                        <label htmlFor="password" style={{color:'red'}}>Contraseña no válida</label>:
                        <label htmlFor="password">Contraseña</label>}
                    </div>
                    <div className="control-formulario">
                        <button className="btn btn-dark" onClick={(e)=>{e.preventDefault(); iniciarSesion();}}>Iniciar Sesión</button>
                    </div>
                    <div className="control-formulario">
                        <p>No tengo cuenta<button className="btn btn-link" onClick={()=>{props.history.push("/registro")}}>Registro</button></p>
                    </div>
                    {hayError && <div className="control-formulario error">
                        { mensajeError }
                    </div>}
                </div>
            </form>
           </>
}

export default Login;