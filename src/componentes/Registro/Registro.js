import React, { useState } from "react";
import '../login/Login.css';
import { registrarNuevoUsuarioServidor } from "../../FuncionesFirebase";


const Registro = (props)=>{

    const [listaErrores,setListaErrores] = useState([]);
    const [nombre,setNombre] = useState('');
    const [correo,setCorreo] = useState('');
    const [contrasena,setContrasena] = useState('');
    const [hayError,setHayError] = useState(false);
    const [mensajeError,setMensajeError] = useState('');

    const expresionRegular = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    const registroNuevoUsuario = async()=>{
        if(nombre.trim().length < 3){
            listaErrores.push("nombre");
            setHayError(true);
        }
        if(!validarCorreo(correo)){
            listaErrores.push("correo");
            setHayError(true);
        }
        if(contrasena.trim().length < 6){
            listaErrores.push("contraseña");
            setHayError(true);
        }
        if(listaErrores.length === 0){
            const resultado = await registrarNuevoUsuarioServidor(correo,contrasena,nombre);
            if(!resultado.exito){
                setMensajeError(resultado.codigoError);
                setHayError(true);
                quitarMensaje();
            }
        }else{
            quitarMensaje();
        }
    }

    function validarCorreo(email){
        return expresionRegular.test(email);
    }
    
    function quitarMensaje(){
        setTimeout(() => {
            setHayError(false);
            setListaErrores([]);
            setMensajeError('');
        }, 2000);
    }

    return <>
            <form className="formulario">
                <div className="formulario-contenido">
                <div className="control-formulario form-floating mb-3">
                        <input 
                        className={listaErrores.indexOf("nombre") !== -1 ? "form-control is-invalid":"form-control"}
                        type="text" 
                        id="nombre" 
                        placeholder="Nombre"
                        value={nombre} 
                        onChange={(e)=>{setNombre(e.target.value)}}/>
                        {listaErrores.includes("nombre") ? 
                        <label htmlFor="nombre" style={{color:'red'}}>Nombre Inválido</label>:
                        <label htmlFor="nombre">Nombre</label>}
                    </div>
                    <div className="control-formulario form-floating mb-3">
                        <input 
                        className={listaErrores.indexOf("correo") !== -1 ? "form-control is-invalid":"form-control"}
                        type="email" 
                        id="correo" 
                        placeholder="Correo"
                        value={correo} 
                        onChange={(e)=>{setCorreo(e.target.value)}}/>
                        {listaErrores.includes("correo") ? 
                        <label htmlFor="correo" style={{color:'red'}}>Correo Inválido</label>:
                        <label htmlFor="correo">Correo</label>}
                    </div>
                    <div className="control-formulario form-floating mb-3">
                        <input 
                        className={listaErrores.indexOf("contraseña") !== -1 ? "form-control is-invalid":"form-control"}
                        type="password" 
                        id="password" 
                        placeholder="Contraseña"
                        value={contrasena} 
                        onChange={(e)=>{setContrasena(e.target.value)}}/>
                        {listaErrores.includes("contraseña") ? 
                        <label htmlFor="password" style={{color:'red'}}>Contraseña no válida</label>:
                        <label htmlFor="password">Contraseña</label>}
                    </div>
                    <div className="control-formulario">
                        <button className="btn btn-dark" onClick={(e)=>{e.preventDefault(); registroNuevoUsuario();}}>Registrarse</button>
                    </div>
                    <div className="control-formulario">
                        <p>Ya tengo cuenta<button className="btn btn-link" onClick={()=>{props.history.push("/login")}}>Iniciar Sesión</button></p>
                    </div>
                    {hayError && <div className="control-formulario error">
                        {mensajeError }
                    </div>}
                </div>
            </form>
           </>
}

export default Registro;