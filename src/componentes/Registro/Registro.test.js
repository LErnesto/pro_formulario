import { fireEvent, render, screen } from "@testing-library/react";
import Registro from "./Registro";

let nombre;
let correo;
let contrasenia;
beforeEach(()=>{
    render(<Registro/>);
    nombre = screen.getByLabelText('Nombre');
    correo = screen.getByLabelText('Correo');
    contrasenia = screen.getByLabelText('Contraseña');
});

describe('renderizado y interaccion en registro',()=>{
    it('creacion de controles en registro',()=>{
        expect(nombre.value).toBe('');
        expect(correo.value).toBe('');
        expect(contrasenia.value).toBe('');
        const btns = screen.getAllByRole('button');
        expect(btns.length).toBe(2);
    });
    it('entrada de datos',()=>{
        fireEvent.change(nombre,{target:{ value: 'un nombre'}});
        fireEvent.change(correo,{target:{ value: 'un correo'}});
        fireEvent.change(contrasenia,{target:{ value: '123456'}});
        expect(nombre.value).toBe('un nombre');
        expect(correo.value).toBe('un correo');
        expect(contrasenia.value).toBe('123456');
    });
    it('entrada de correo incorrecto',()=>{
        fireEvent.change(nombre,{target:{ value: 'un nombre'}});
        fireEvent.change(correo,{target:{ value: 'un correo incorrecto'}});
        fireEvent.change(contrasenia,{target:{ value: '123456'}});
        fireEvent.click(screen.getByText('Registrarse'));
        expect(screen.getByText(/Correo Invalido/i)).toBeInTheDocument();
    });
    it('entrada de contraseña invalida',()=>{
        fireEvent.change(nombre,{target:{ value: 'un nombre'}});
        fireEvent.change(correo,{target:{ value: 'correo@correo.com'}});
        fireEvent.change(contrasenia,{target:{ value: '1234'}});
        fireEvent.click(screen.getByText('Registrarse'));
        expect(screen.getByText(/contraseña no valida/i)).toBeInTheDocument();
    });
    it('entrada de nombre incorrecto por longitud de 2',()=>{
        fireEvent.change(nombre,{target:{ value: 'un'}});
        fireEvent.change(correo,{target:{ value: 'correo@correo.com'}});
        fireEvent.change(contrasenia,{target:{ value: '123456'}});
        fireEvent.click(screen.getByText('Registrarse'));
        expect(screen.getByText(/nombre invalido/i)).toBeInTheDocument();
    });
});