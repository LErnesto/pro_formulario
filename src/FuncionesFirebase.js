import { collection, addDoc, doc, setDoc, deleteDoc, getDocs, getDoc } from "firebase/firestore";
import { ref, uploadBytes, getDownloadURL, deleteObject  } from "firebase/storage";
import { signInWithEmailAndPassword, createUserWithEmailAndPassword } from "firebase/auth";
import { db, db_file, auth } from "./Firebase";

const iniciarSesionServidor = async(correo, contrasena)=>{
    let resultado;
    await signInWithEmailAndPassword(auth, correo, contrasena)
        .then(() => {
            resultado = {exito:true};
        })
        .catch((error) => {
            resultado = {
                exito:false,
                codigoError:error.code
            }
        });
    return resultado;
}

const registrarNuevoUsuarioServidor = async(correo, contrasena, nombre)=>{
    let resultado;
    await createUserWithEmailAndPassword(auth, correo, contrasena)
            .then(async (userCredential) => {
                const user = userCredential.user;
                await setDoc(doc(db, "usuarios", user.uid), {
                    nombre: nombre
                  });
                resultado = {exito:true}
            })
            .catch((error) => { 
                resultado = {
                    exito:false,
                    codigoError:error.code
                }
            });
    return resultado;
}

const hayUsuarioLogeado = async(user)=>{
    const uid = user.uid;
    const docRef = doc(db, "usuarios", uid);
    const docSnap = await getDoc(docRef);
    const datosUsuario = {
        id:uid,
        correo:user.email,
        nombre:docSnap.data().nombre
      }
    return datosUsuario;
}

const registrarNuevoAspirante = async(id, curriculumVitae, aspirante)=>{
    try{
        const storageref = ref(db_file, id + '/cv/' + curriculumVitae.name);
        await uploadBytes(storageref,curriculumVitae);
        const url = await getDownloadURL(storageref);
        await addDoc(collection(db, id), {
            ...aspirante,
            urlCurriculum:url
        });
        return {exito:true};
    }catch (e) {
        return {exito:false, error:e};
    }
}

const consultaAspirante = async(id, idAspirante)=>{
    const docRef = doc(db, id, idAspirante);
    const docSnap = await getDoc(docRef);
    const datosAspirante = docSnap.data();
    return datosAspirante;
}

const actualizarAspirante = async(id, idAspirante, aspirante, curriculum = null)=>{
    let resultado;
    try {
        let url = aspirante.urlCurriculum;
        let nombreCurriculum = aspirante.nombreCurriculum;
        if(curriculum !== null){
            const objetoReferencia = ref(db_file, id + '/cv/' + nombreCurriculum);
            await deleteObject(objetoReferencia);
            const referenciaStorage = ref(db_file, id + '/cv/' + curriculum.name);
            await uploadBytes(referenciaStorage,curriculum);
            url = await getDownloadURL(referenciaStorage);
            nombreCurriculum = curriculum.name;
        }

        await setDoc(doc(db, id, idAspirante), {
            ...aspirante,
            urlCurriculum:url,
            nombreCurriculum:nombreCurriculum
        });
        resultado = {exito:true};
      } catch (e) {
          resultado = {exito:false, error:e}
      }
      return resultado;
}

const eliminarAspirante = async (id, idAspirante, nombreCv)=>{
    await deleteDoc(doc(db, id, idAspirante));
    const refcv = ref(db_file, id + "/cv/" + nombreCv);
    await deleteObject(refcv);
}

const obtenerListaAspirantes = async (id)=>{
    const documetos = await getDocs(collection(db,id));
    let listaAuxiliar = [];
    documetos.forEach(item=>{
        listaAuxiliar.push({
        id:item.id,
        ...item.data()
        });
    });
    listaAuxiliar = listaAuxiliar.sort((a, b)=>{
        if (a.nombre > b.nombre) {
            return 1;
          }
          if (a.nombre < b.nombre) {
            return -1;
          }
          return 0;
    });
    return listaAuxiliar;
}
export {iniciarSesionServidor, registrarNuevoUsuarioServidor, hayUsuarioLogeado,
     registrarNuevoAspirante, eliminarAspirante, actualizarAspirante,
     obtenerListaAspirantes, consultaAspirante};